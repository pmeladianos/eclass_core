/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.params;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Midas This class contains all static parameters that will not change
 * by the administrator when the system is online
 *
 * All the parameters that start with LINK_* are basically string patterns that
 * help the parser collect metadata and documents.
 *
 *
 */
public class System_Properties {

    /**
     * Modules String Pattern
     */

    //public static String SERVER_IP="83.212.204.91";
    public static String SERVER_IP;
    public static String SERVER_PORT;
    public static String LINK_MODULES = "modules/auth/";
    public static String ECLASS_SEED_LIST_FILE = "seed_list.txt";
    private List<String> ECLASS_SEED_LIST;

    /**
     * Courses String Pattern
     */
    public static String LINK_COURSE_MAIN = "courses/";
    /**
     * Information page String Pattern
     */
    public static String LINK_VERSION = "info/about.php";
    /**
     * Description page String Pattern
     */
    public static String LINK_DESCRIPTION = "modules/course_description/?course=";
    /**
     * Course Catalogs String Pattern
     */
    public static String LINK_CATALOG_COURSES = LINK_MODULES + "listfaculte.php";
    /**
     * Course info String Pattern
     */
    public static String LINK_COURSE = "modules/document/document.php?course=";
    /**
     * Document module String Pattern
     */
    public static String LINK_COURSE_DOCUMENT = "modules/document/document.php?course=";
    /**
     * Document module String Pattern for Version 3
     */
    public static String LINK_COURSE_DOCUMENTV3 = "modules/document/?course=";
    /**
     * Default Solr Server url
     */
    public static String SOLR_URI = "http://"+SERVER_IP+":"+SERVER_PORT+"/solr/eclass_core";

    /**
     * Delay between http requests. Very small value means that the eclass
     * servers will receive many requests per second and they may block traffic
     * from the crawler
     */
    public static Integer HTTP_DELAY = 1000;
    /**
     * Default Mongo DB Server
     */
    public static String OBJ_DB = "localhost";
    /**
     * Default Mongo DB Server Port
     */
    public static Integer OBJ_DB_PORT = 27017;
    /**
     * Default Path of the file Server
     */
    public static String FILESERVERPATH = System.getProperty("user.home") + "/eclassDocuments/";
    /**
     * Default Path of the school_faculty information text file output
     */
    public static String SCHOOL_FACULTY_PATH = System.getProperty("user.home") + "/school_faculty.txt";
    /**
     * Default Path of the faculty_course information text file output
     */
    public static String FACULTY_COURSE_PATH = System.getProperty("user.home") + "/faculty_course.txt";

    /**
     * If the collector is in a different machine than the SOLR server it will connect through FTP in order to save the documents in the Fileserver and so this parameter should be true
     */
    public static Boolean USE_FTP = true;
    /**
     * IP of the Ftp FileServer
     */
    public static String SFTPHOST ;
    /**
     * Port of the Ftp fileServer (Default 22)
     */
    public static int SFTPPORT = 22;
    /**
     * Username Credential to connect via ftp to the server
     */
    public static String SFTPUSER ;
    /**
     * Password Credential to connect via ftp to the server
     */
    public static String SFTPPASS ;
    /**
     * Location where the documents will be saved in the fileServer
     */
    public static String SFTPWORKINGDIR = "/home/"+SFTPUSER+"/eclassDocuments/";
    public static Boolean COMMIT = true;
    public static String STATS_EXPORT_PATH = System.getProperty("user.home")+"/";
    public static String CREDENTIALS_HEAL_AUEB;
    public static String HEAL_API_URI;
    public static String HEAL_API_CATEGORIES;
    public static String PUBLISHERS_PATH;
    public static String CLASSIFICATIONS_PATH;
    public static String TARGET_CRAWL;
    public static String SOLRADMINUNAME;
    public static String SOLRADMINPASS;
    public void init() {
        Field[] d = this.getClass().getFields();
        Properties props = new Properties();
        FileInputStream fis = null;

        try {
            fis = new FileInputStream("crawler_properties.properties");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            props.load(fis);
        } catch (IOException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Field field : d) {
            try {
                if (field.getType().toString().contains("Integer")) {
                    field.set(this, Integer.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Boolean")) {
                    field.set(this, Boolean.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Long")) {
                    field.set(this, Long.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Double")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Float")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("String")) {
                    field.set(this, props.getProperty((field.getName())));
                }

            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                System.out.println("Parameter loading Error");
                e.printStackTrace();
            } catch (IllegalAccessException ex) {
                Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        LINK_CATALOG_COURSES = LINK_MODULES + LINK_CATALOG_COURSES;
        SOLR_URI = "http://"+SERVER_IP+":"+SERVER_PORT+SOLR_URI;
        SFTPWORKINGDIR = "/home/"+SFTPUSER+SFTPWORKINGDIR;
        String relDir=System.getProperty("user.home");
        FILESERVERPATH = relDir + FILESERVERPATH;
        STATS_EXPORT_PATH=relDir+STATS_EXPORT_PATH;
        PUBLISHERS_PATH = relDir + PUBLISHERS_PATH;
        CLASSIFICATIONS_PATH = relDir + CLASSIFICATIONS_PATH;
        SCHOOL_FACULTY_PATH = relDir + SCHOOL_FACULTY_PATH;
        FACULTY_COURSE_PATH = relDir + FACULTY_COURSE_PATH;

        if (!ECLASS_SEED_LIST_FILE.equals("")) {
            try {
                ECLASS_SEED_LIST = Files.readAllLines(Paths.get(ECLASS_SEED_LIST_FILE));
            } catch (IOException ex) {
                Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public List<String> getECLASS_SEED_LIST() {
        return ECLASS_SEED_LIST;
    }

}
