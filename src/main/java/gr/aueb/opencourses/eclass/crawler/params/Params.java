/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler.params;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Midas This class contains all static parameters that will not change
 * by the administrator when the system is online
 *
 * All the parameters that start with LINK_* are basically string patterns that
 * help the parser collect metadata and documents.
 *
 *
 */
public class Params {

    /**
     * Modules String Pattern
     */
    
    //public static String SERVER_IP="83.212.204.91";
    public static String SERVER_IP="83.212.170.56";
    public static String SERVER_PORT="8983";
    public static String LINK_MODULES = "modules/auth/";
    public static String SEEDS = System.getProperty("user.home")+"seeds.txt";
    /**
     * Courses String Pattern
     */
    public static String LINK_COURSE_MAIN = "courses/";
    /**
     * Information page String Pattern
     */
    public static String LINK_VERSION = "info/about.php";
    /**
     * Description page String Pattern
     */
    public static String LINK_DESCRIPTION = "modules/course_description/?course=";
    /**
     * Course Catalogs String Pattern
     */
    public static String LINK_CATALOG_COURSES = LINK_MODULES + "listfaculte.php";
    /**
     * Course info String Pattern
     */
    public static String LINK_COURSE = "modules/document/document.php?course=";
    /**
     * Document module String Pattern
     */
    public static String LINK_COURSE_DOCUMENT = "modules/document/document.php?course=";
    /**
     * Document module String Pattern for Version 3
     */
    public static String LINK_COURSE_DOCUMENTV3 = "modules/document/?course=";
    /**
     * Default Solr Server url
     */
    public static String SOLR_URI = "http://"+SERVER_IP+":"+SERVER_PORT+"/solr/eclass_core";

    /**
     * Delay between http requests. Very small value means that the eclass
     * servers will receive many requests per second and they may block traffic
     * from the crawler
     */
    public static Integer HTTP_DELAY = 1500;
    /**
     * Default Mongo DB Server
     */
    public static String OBJ_DB = "localhost";
    /**
     * Default Mongo DB Server Port
     */
    public static Integer OBJ_DB_PORT = 27017;
    /**
     * Default Path of the file Server
     */
    public static String FILESERVERPATH = System.getProperty("user.home") + "/eclassDocuments/";
    /**
     * Default Path of the school_faculty information text file output
     */
    public static String school_faculty_path = System.getProperty("user.home") + "/school_faculty.txt";
    /**
     * Default Path of the faculty_course information text file output
     */
    public static String faculty_course_path = System.getProperty("user.home") + "/faculty_course.txt";

    /**
     * If the collector is in a different machine than the SOLR server it will connect through FTP in order to save the documents in the Fileserver and so this parameter should be true
     */
    public static Boolean USE_FTP = true;
    /**
     * IP of the Ftp FileServer
     */
    public static String SFTPHOST = "83.212.170.58";
    /**
     * Port of the Ftp fileServer (Default 22)
     */
    public static int SFTPPORT = 22;
    /**
     * Username Credential to connect via ftp to the server
     */
    public static String SFTPUSER = "polmel";
    /**
     * Password Credential to connect via ftp to the server
     */
    public static String SFTPPASS = "40qww294e";
    /**
     * Location where the documents will be saved in the fileServer
     */
    public static String SFTPWORKINGDIR = "/home/"+SFTPUSER+"/eclassDocuments/";
    public static Boolean COMMIT = true;
    public static String stats_export_path = System.getProperty("user.home");
    public static String CREDENTIALS_HEAL_AUEB="username=aueb&signature=ad0ebfa979666d5905bf2e664982d1bcbfcd78349a676f4920de82110282976bbfe047acfc5a0352eabb362cad3ed8e7d209104ffe8385bb0914d04ceab2164558dcf7cdf53fddb44864ba67c7540a40fdacf2854e2418170270a12dd4d0ec2834c60a816edb4d6944f4fd10aa253eae03f006c27f95922fef76e8e9c8cda9423beefc63b6c15b87e4a5f7d3edb497ed86a5be75896b981eeab14290b546283b5728569dbba0eb7264c6a724caaab8dcc80a38fcddc5d0fd2bde88d2f38768bc30e2470b640d0c6bb79159c31b50fbdb13f5d33ca0054d58cea0b4c38ff69c30a2fd6edc202239d17b5772d49d511091c0afae10d3ff210df6b0c202ab044f74";
    public static String HEAL_API_URI="https://www.heal-link.gr/api_ex/journals/subcategory/load/";
    public static String HEAL_API_CATEGORIES="https://www.heal-link.gr/api_ex/journals/categories?";
    public static String publishers_path = System.getProperty("user.home") + "/publishers.txt";
    public static String classifications_path = System.getProperty("user.home") + "/classifications.txt";
    public static String TARGET_CRAWL="ECLASS";//HEAL_LINK
    public static String SOLRADMINUNAME="admin";
    public static String SOLRADMINPASS="40qww294e";
    public void init() {
        Field[] d = this.getClass().getFields();
        Properties props = new Properties();
        FileInputStream fis = null;

        try {
            fis = new FileInputStream("crawler_properties.prop");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            props.load(fis);
        } catch (IOException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Field field : d) {
            try {
                if (field.getType().toString().contains("Integer")) {
                    field.set(this, Integer.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Boolean")) {
                    field.set(this, Boolean.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Long")) {
                    field.set(this, Long.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Double")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Float")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("String")) {
                    field.set(this, props.getProperty((field.getName())));
                }

            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                System.out.println("Parameter loading Error");
                e.printStackTrace();
            } catch (IllegalAccessException ex) {
                Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
