/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.eclass.crawler.handlers.EclassVersion2Handler;
import gr.aueb.opencourses.eclass.crawler.handlers.EclassVersion3Handler;
import gr.aueb.opencourses.eclass.crawler.handlers.VersionHandler;
import gr.aueb.opencourses.params.System_Properties;
import gr.aueb.opencourses.structures.solr.SolrDocumentGenerator;
import grph.Grph;
import grph.in_memory.InMemoryGrph;
import grph.properties.StringProperty;
import org.apache.solr.common.SolrInputDocument;
import org.apache.tika.exception.TikaException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static gr.aueb.opencourses.eclass.crawler.Utils.getJsoupDoc;

/**
 * This class contains all methods required to parse the content from the eclass
 * web pages that are specified as in the System properties and seed_list files
 * It basically performs a depth first search for all
 * schools-departments-courses found and saves the information to the
 * appropriate structure(see structures)
 *
 * @author Midas
 */
public class EclassHandler {

    private static final Logger log = Logger.getLogger(EclassHandler.class.getName());
    public static Integer version = 0;
    private String seed;
    private String majorVersion;

    /**
     * Constructor: An instance of this class will handle one seed The system
     * reads the majorVersion that the institution provides in the info page in order
     * to determine how to parse the website. It basically reads the content
     * from top to bottom visiting subsequent pages and saving the data in the
     * appropriate structures
     *
     * @param seed The link of the main page of the eclass we want to crawl
     */
    EclassHandler(String seed) throws IOException, MalformedURLException, TikaException, InterruptedException, Exception {
        this.seed = seed;
        Document info_page = getJsoupDoc(this.seed + System_Properties.LINK_VERSION);
        //Document info_page = Jsoup.parse(new URL(this.seed + System_Properties.LINK_VERSION).openStream(), "UTF-8", this.seed + System_Properties.LINK_VERSION);
        Elements version_attribute = info_page.getElementsByAttributeValue("href", "http://www.openeclass.org/");
        String versionString = version_attribute.text().replaceAll("[^\\d.]", "");
        VersionHandler handler = null;
        if (versionString.startsWith("3")) {
            String z = "000";
            this.majorVersion = "3";
            versionString = versionString.replaceAll("\\.", "") + z;
            versionString = versionString.substring(0, 3);
            //TODO Subversion handlers >3.2.2
            Integer intVersionRaw = Integer.valueOf(versionString);
            if (intVersionRaw > 322) {
                this.version = intVersionRaw;
            }
            handler = new EclassVersion3Handler();
            //System_Properties.LINK_CATALOG_COURSES="modules/auth/opencourses.php";
        } else if (versionString.startsWith("2")) {
            this.majorVersion = "2";
            handler = new EclassVersion2Handler();
        }

        Elements name_attribute = info_page.getElementsByAttributeValue("class", "mainpage");
        String eclass_school = name_attribute.text();
        if (eclass_school.isEmpty())
            throw new Exception();
        SolrInputDocument docsc = new SolrDocumentGenerator().getSolrSchoolDocument(eclass_school, seed);
        Utils.commit(docsc);
        String url = seed + System_Properties.LINK_CATALOG_COURSES;
        String genericUrl = seed + System_Properties.LINK_MODULES + "opencourses.php?fc=";
        boolean departmentIdStart = false;
        boolean departmentIdEnd = false;
        int threshold = 100;
        int sumNonValid = 0;
        int id = 0;
        HashMap<Integer, String[]> nodeNameIds = new HashMap<Integer, String[]>();
        HashMap<String, String> facultyNameLinks = new HashMap<String, String>();
        Grph g = new InMemoryGrph();
        grph.properties.StringProperty p = new StringProperty("label");

        while (!departmentIdEnd) {
            Document course_module = getJsoupDoc(genericUrl + id);
            if (course_module.text().contains("ERROR: no faculty with id")) {
                sumNonValid += 1;
                if (departmentIdStart && sumNonValid > threshold)
                    departmentIdEnd = true;
                id++;
            } else {
                departmentIdStart = true;
                sumNonValid = 0;
                Element head_tags;
                Elements links;
                try {
                    head_tags = course_module.getElementsContainingText("Σχολή - Τμήμα").get(course_module.getElementsContainingText("Σχολή - Τμήμα").size() - 1);
                    links = head_tags.select("a[href]");
                } catch (Exception e) {
                    continue;
                }
                for (int idx = 0; idx < links.size(); idx++) {
                    Element link = links.get(idx);
                    String name = link.text();
                    String l = link.attr("href");
                    Integer nodeId = Integer.valueOf(link.attr("href").split("=")[1]);
                    if (!nodeNameIds.containsKey(nodeId)) {
                        String[] vals = {l, name};
                        nodeNameIds.put(nodeId, vals);
                        g.addVertex(nodeId);
                        p.setValue(nodeId, name);
                    }
                    if (idx > 0 && !g.areVerticesAdjacent(Integer.valueOf(links.get(idx - 1).attr("href").split("=")[1]), nodeId)) {
                        Integer parent = Integer.valueOf(links.get(idx - 1).attr("href").split("=")[1]);
                        g.addSimpleEdge(parent, nodeId, true);
                    }
                }
                id++;
            }
        }

        int masterNode = g.getInaccessibleVertices().toIntArray()[0];
        for (Integer v : g.getVertices().toIntArray()) {

            int[] neighboors = g.getOutEdges(v).toIntArray();
            if (this.seed.contains("eclass.uoa.gr")){
                if (g.getShortestPath(masterNode, v).getLength() == 1) {
                    //TODO FIX LEVEL OF INFO

                    //int l1=g.getInNeighbors(v).toIntArray()[0];

                    //int l2 = g.getInNeighbors(l1).toIntArray()[0];
                    String name = nodeNameIds.get(v)[1];
                    //if (g.getShortestPath(masterNode, v).getLength() > 2) {
                    //   int e = g.getEdgesIncidentTo(l2).toIntArray()[0];
                    //    int v1 = g.getTheOtherVertex(e, l2);
                    //     name = nodeNameIds.get(v1)[1] + " - " + nodeNameIds.get(l2)[1];
                    // }
                    System.out.println(name);
                    facultyNameLinks.put(name, nodeNameIds.get(v)[0]);
                }

            }else{
                if (neighboors.length == 0) {
                    //TODO FIX LEVEL OF INFO

                    //int l1=g.getInNeighbors(v).toIntArray()[0];

                    //int l2 = g.getInNeighbors(l1).toIntArray()[0];
                    String name = nodeNameIds.get(v)[1];
                    //if (g.getShortestPath(masterNode, v).getLength() > 2) {
                    //   int e = g.getEdgesIncidentTo(l2).toIntArray()[0];
                    //    int v1 = g.getTheOtherVertex(e, l2);
                    //     name = nodeNameIds.get(v1)[1] + " - " + nodeNameIds.get(l2)[1];
                    // }
                    System.out.println(name);
                    facultyNameLinks.put(name, nodeNameIds.get(v)[0]);
                }

            }


        }
        ClassLoader.getSystemClassLoader().setDefaultAssertionStatus(true);
        g.setVerticesLabel(p);

        for (Map.Entry<String, String> row : facultyNameLinks.entrySet()) {
            String faculty = row.getKey();
            if (faculty.equals("OpenCourses"))
                continue;
            String faculty_link = row.getValue();
            if (this.majorVersion.equals("3")) {
                faculty = faculty.replaceAll("\\P{L}+^\\s", "");
                System.out.println(faculty);
            }

            SolrInputDocument docf = new SolrDocumentGenerator().getSolrFacultyDocument(eclass_school, faculty, faculty_link, seed);
            Utils.commit(docf);

            handler.getCoursesInFaculty(seed, faculty, eclass_school, seed+faculty_link);
        }
    }
}






