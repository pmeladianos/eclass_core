/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler.handlers;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import gr.aueb.opencourses.eclass.crawler.Connector;
import gr.aueb.opencourses.eclass.crawler.EclassHandler;
import gr.aueb.opencourses.eclass.crawler.Utils;
import gr.aueb.opencourses.params.System_Properties;
import gr.aueb.opencourses.structures.Course;
import gr.aueb.opencourses.structures.EclassDocument;
import gr.aueb.opencourses.structures.EclassFolder;
import gr.aueb.opencourses.structures.solr.SolrDocumentGenerator;
import org.apache.solr.common.SolrInputDocument;
import org.apache.tika.exception.TikaException;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Midas
 */
public class EclassVersion3Handler extends VersionHandler {

    private static final Logger log = Logger.getLogger(EclassVersion2Handler.class.getName());
    private String seed;

    @Override
    public void getCoursesInFaculty(String seed, String faculty, String eclass_school, String link) throws IOException, InterruptedException {
        this.seed = seed;
        Document courses_categories_faculty = Utils.getJsoupDoc(link);

        Elements department_course_categories = courses_categories_faculty.getElementsByAttributeValueStarting("href", "opencourses.php?fc=");

        for (Element course_category : department_course_categories) {
            try {
                String courses_level_url = this.seed + System_Properties.LINK_MODULES + course_category.attr("href");

                Document courses_in_faculty = Utils.getJsoupDoc(courses_level_url);
                Elements table_list_courses = courses_in_faculty.getElementsByAttributeValue("class", "table-default");
                Elements course_rows = null;
                try {
                    for (Element tEl:table_list_courses){
                        if(tEl.text().contains("Όνομα Μαθήματος (κωδικός)")) {
                            course_rows = tEl.select("tr");
                            break;
                        }
                    }
                    //course_rows = table_list_courses.get(0);
                } catch (Exception e) {
                    //e.printStackTrace();
                    continue;
                }
                for (Element course_row : course_rows) {
                    Elements tds = course_row.select("td");
                    if (tds.isEmpty()) {
                        continue;
                    }
                    String course_type = getCourseType(tds);
                    if (course_type.toLowerCase().contains("ανοικτό") || course_type.toLowerCase().contains("open")) {
                        String course_id = tds.get(0).select("a").attr("href").substring(6).split("/")[1];
                        Course c = null;
                        try {
                            String name = tds.get(0).text();

                            TimeLimiter limiter = new SimpleTimeLimiter();
                            c = limiter.callWithTimeout(new Callable<Course>() {
                                public Course call() throws IOException, MalformedURLException, TikaException, InterruptedException {
                                    return getCourseInfo(course_id);
                                }
                            }, 180, TimeUnit.MINUTES, false);

                            c.setId(course_id);
                            c.setLink(this.seed + System_Properties.LINK_COURSE_MAIN + course_id);
                            c.setName(name);
                            c.setTeacher(tds.get(1).text());

                            String course_info_link = "";
                            course_info_link = this.seed + System_Properties.LINK_DESCRIPTION + c.getId();
                            Document course_info_page = null;
                            course_info_page = Utils.getJsoupDoc(course_info_link);
                            Elements course_content_html = null;
                            course_content_html = course_info_page.getElementsByAttributeValue("class", "panel panel-action-btn-default");

                            String course_desc = "Το μάθημα δεν διαθέτει περιγραφή";
                            if (course_content_html != null) {
                                try {
                                    course_desc = course_content_html.get(0).text();
                                } catch (Exception e) {
                                    course_desc = "Το μάθημα δεν διαθέτει περιγραφή";
                                }
                            }
                            c.setCourseDescription(course_desc);
                            c.setFaculty(faculty);
                            c.setSchool(eclass_school);
                            SolrInputDocument docc = new SolrDocumentGenerator().getSolrCourseDocument(c);
                            Utils.commit(docc);

                            final String sc = eclass_school;
                            final String fc = faculty;
                            final Course cc = c;
                            limiter = new SimpleTimeLimiter();
                            limiter.callWithTimeout(new Callable<Boolean>() {
                                public Boolean call() throws IOException, MalformedURLException, TikaException, InterruptedException {
                                    return Utils.commitCourseInfoSolr(sc, fc, cc);
                                }
                            }, 30, TimeUnit.MINUTES, false);

                            //commitCourseInfoSolr(eclass_school,faculty,c);
                        } catch (Exception e) {
                            log.log(Level.SEVERE, e.toString());
                            e.printStackTrace();
                            continue;
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                continue;
            }
        }

    }

    private Course getCourseInfo(String course_id) throws MalformedURLException, IOException, TikaException, InterruptedException {
        log.log(Level.INFO, "Getting course Info for: " + course_id);
        String docs_link = "";
        Document course_page = null;
        Elements rows = null;

        docs_link = seed + System_Properties.LINK_COURSE_DOCUMENTV3 + course_id;
        course_page = Utils.getJsoupDoc(docs_link);
        //String title = getCourseTitle(course_page.select("title").text());
        rows = course_page.body().getElementsByAttributeValue("class", "table-default").select("tr");

        int count = 0;
        Course course = new Course();
        //course.setName(title);

        for (Element row : rows) {

            if (count == 0) {
                count++;
                continue;
            }

            Elements tds = row.select("td");
            boolean isFolder = false;
            boolean isDocument = false;
            String type = tds.get(2).text();
            if (type.length() == 1) {
                isFolder = true;
            } else {
                isDocument = true;
            }
            if (isDocument) {
                EclassDocument d = new EclassDocument();
                String temp = tds.get(1).select("a").text();
                d.setLink(tds.get(1).select("a").attr("href"));

                d.setName(tds.get(1).select("a").attr("title"));
                try {
                    d.setFileServerlink(Utils.archive(d.getLink(), Connector.ftp));

                    String localLink = System.getProperty("user.home") + "/eclassDocuments/" + d.getFileServerlink();

                    File file = new File(localLink);
                    d.setContent(Utils.parseToPlainText(file));
                    course.getDocuments().add(d);
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.toString());
                    continue;
                }

            }
            if (isFolder) {
                EclassFolder f = getFolderInfo(tds.get(1).select("a").attr("href"));
                f.setName(tds.get(1).select("a").attr("title"));

                course.getFolders().add(f);
            }

        }

        return course;
    }

    private EclassFolder getFolderInfo(String link) throws MalformedURLException, IOException, TikaException, InterruptedException {
        link = seed + link;
        EclassFolder f = new EclassFolder();
        Thread.sleep(System_Properties.HTTP_DELAY);
        Document course_page = Utils.getJsoupDoc(link);
        Elements rows = null;

        rows = course_page.body().getElementsByAttributeValue("class", "table-default").select("tr");

        int count = 0;
        for (Element row : rows) {
            if (count == 0) {
                count++;
                continue;
            }
            Elements tds = row.select("td");
            boolean isFolder = false;
            boolean isDocument = false;
            String type = "";
            try {
                type = tds.get(2).text();
            } catch (Exception e) {
                continue;
            }
            if (type.length() == 1) {
                isFolder = true;
            } else {
                isDocument = true;
            }

            if (isDocument) {
                EclassDocument d = new EclassDocument();
                d.setLink(tds.get(1).select("a").attr("href"));
                d.setName(tds.get(1).select("a").attr("title"));
                try {
                    d.setFileServerlink(Utils.archive(d.getLink(), Connector.ftp));

                    String localLink = System.getProperty("user.home") + "/eclassDocuments/" + d.getFileServerlink();

                    File file = new File(localLink);
                    d.setContent(Utils.parseToPlainText(file));
                    f.getDocuments().add(d);
                } catch (Exception e) {
                    log.log(Level.SEVERE, e.toString());
                    continue;
                }
            }
            if (isFolder) {
                EclassFolder f1 = getFolderInfo(tds.get(1).select("a").attr("href"));
                f1.setName(tds.get(1).select("a").attr("title"));
                f.getFolders().add(f1);
            }
        }
        return f;
    }

    private String getCourseType(Elements tds){
        if (EclassHandler.version<322){
            return tds.get(2).select("img").attr("title");
        }
        else{
            if(tds.get(2).toString().contains("unlock"))
                return "open";
        }
        return "closed";     
    }
    private String getCourseTitle(String rawTitle){
        if (EclassHandler.version<322){
            return rawTitle.split("\\|")[1];
        }
        else if (EclassHandler.version>=330){
            return rawTitle.split("\\|")[2];
        }
        return rawTitle.split("\\|")[1];     
    }
}
