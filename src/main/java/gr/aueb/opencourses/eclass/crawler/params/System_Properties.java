/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler.params;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Midas This class reads all properties that define how the crawler
 * will behave It also loads the initial eClass sites that will be crawled The
 * file location which contains the seed list is specified in the
 * crawler_properties.prop file
 *
 */
public class System_Properties {

    public String ECLASS_SEED_LIST_FILE;

    private List<String> ECLASS_SEED_LIST;

    /**
     * This methods returns the seed list defined by the user
     */
    public List<String> getECLASS_SEED_LIST() {
        return ECLASS_SEED_LIST;
    }

    /**
     * This methods loads the parameters defined in this class from the
     * "crawler_properties.prop" file which must be located in the same folder
     * as the jar
     */
    public void init() {
        Field[] d = this.getClass().getFields();
        Properties props = new Properties();
        FileInputStream fis = null;

        try {
            fis = new FileInputStream("crawler_properties.prop");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            props.load(fis);
        } catch (IOException ex) {
            Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (Field field : d) {
            try {
                if (field.getType().toString().contains("Integer")) {
                    field.set(this, Integer.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Boolean")) {
                    field.set(this, Boolean.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Long")) {
                    field.set(this, Long.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Double")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("Float")) {
                    field.set(this, Double.valueOf(props.getProperty((field.getName()))));
                } else if (field.getType().toString().contains("String")) {
                    field.set(this, props.getProperty((field.getName())));
                }

            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                System.out.println("Parameter loading Error");
                e.printStackTrace();
            } catch (IllegalAccessException ex) {
                Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (!ECLASS_SEED_LIST_FILE.equals("")) {
            try {
                ECLASS_SEED_LIST = Files.readAllLines(Paths.get(ECLASS_SEED_LIST_FILE));
            } catch (IOException ex) {
                Logger.getLogger(System_Properties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
