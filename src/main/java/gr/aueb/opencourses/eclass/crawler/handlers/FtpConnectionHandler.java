/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler.handlers;

import com.jcraft.jsch.*;
import gr.aueb.opencourses.params.System_Properties;

import java.io.File;
import java.io.FileInputStream;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Midas
 */
public class FtpConnectionHandler {

    Session session = null;
    Channel channel = null;
    ChannelSftp channelSftp = null;
    private static final Logger log = Logger.getLogger(FtpConnectionHandler.class.getName());

    public  boolean setupConnection() throws JSchException {

                JSch jsch = new JSch();
                session = jsch.getSession(System_Properties.SFTPUSER, System_Properties.SFTPHOST, System_Properties.SFTPPORT);
                session.setPassword(System_Properties.SFTPPASS);
                java.util.Properties config = new java.util.Properties();
                config.put("StrictHostKeyChecking", "no");
                session.setConfig(config);
                session.connect();
                channel = session.openChannel("sftp");
                channel.connect();
                channelSftp = (ChannelSftp) channel;
                return true;

    }

    public boolean disconnect() {
        try {
            this.channelSftp.disconnect();
            return true;
        } catch (Exception e) {
            log.log(Level.SEVERE, null, e);
            return false;
        }
    }

    public Session getSession() {
        return session;
    }

    public Channel getChannel() {
        return channel;
    }

    public ChannelSftp getChannelSftp() {
        return channelSftp;
    }

    public Boolean commitMetaFiles(){
        //testFtpSession();
        try {
            channelSftp.cd(System_Properties.SFTPWORKINGDIR);
        } catch (SftpException e) {
            e.printStackTrace();
        }
        return Boolean.FALSE;
    }

    public  String commitLocal2FileServer(String rootFolder,String courseFolder,String relativePath){

        try {
            testFtpSession();
            channelSftp.cd(System_Properties.SFTPWORKINGDIR);
            
            Vector<ChannelSftp.LsEntry> fac_folders = channelSftp.ls(System_Properties.SFTPWORKINGDIR);
            boolean folderExists = false;
            for (ChannelSftp.LsEntry entry : fac_folders) {
                if (entry.getFilename().equals(rootFolder)) {
                    folderExists = true;
                    break;
                }
            }
            if (!folderExists) {
                channelSftp.mkdir(System_Properties.SFTPWORKINGDIR + rootFolder);
            }

            channelSftp.cd(System_Properties.SFTPWORKINGDIR + rootFolder);

            Vector<ChannelSftp.LsEntry> fac_courses = channelSftp.ls(System_Properties.SFTPWORKINGDIR + rootFolder);
            folderExists = false;
            for (ChannelSftp.LsEntry entry : fac_courses) {
                if (entry.getFilename().equals(courseFolder)) {
                    folderExists = true;
                    break;
                }
            }
            if (!folderExists) {
                channelSftp.mkdir(System_Properties.SFTPWORKINGDIR + rootFolder + "/" + courseFolder);
            }

            channelSftp.cd(System_Properties.SFTPWORKINGDIR + rootFolder + "/" + courseFolder + "/");
            File f = new File(System_Properties.FILESERVERPATH + relativePath);
            FileInputStream stream = new FileInputStream(f);
            channelSftp.put(stream, f.getName());
            stream.close();
            return relativePath;
        } catch (Exception ex) {
            Logger.getLogger(FtpConnectionHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    
    }
    
    
    
    private Session testFtpSession() throws JSchException {
    try {
        ChannelExec testChannel = (ChannelExec) session.openChannel("exec");
        testChannel.setCommand("true");
        testChannel.connect();
        testChannel.disconnect();
    } catch (Throwable t) {
        log.log(Level.WARNING,null,t);
        setupConnection();
    }
    return session;
}
}
