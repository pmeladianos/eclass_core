/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.structures.Concept;
import gr.aueb.opencourses.structures.solr.SolrDocumentGenerator;
import org.apache.solr.common.SolrInputDocument;

/**
 *
 * @author pmeladianos
 */
public class ConceptHandler {

    public ConceptHandler() {
        
        Concept c=new Concept("algorithms","Shortest path algorithm, dijkstra's algorithm");
        SolrInputDocument docc = new SolrDocumentGenerator().getSolrConceptDocument(c);
        
        Utils.commit(docc);
        
    }
    
}