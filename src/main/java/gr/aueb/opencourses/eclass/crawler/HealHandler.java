/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.params.System_Properties;
import gr.aueb.opencourses.structures.Journal;
import gr.aueb.opencourses.structures.solr.SolrDocumentGenerator;
import org.apache.solr.common.SolrInputDocument;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

/**
 *
 * @author pmeladianos
 */
public class HealHandler {

    HealHandler() throws InterruptedException, IOException {
        ArrayList<String> categories = new ArrayList<String>();
        HashMap<String, Journal> jcl = new HashMap<String, Journal>();
        String urlCategories = System_Properties.HEAL_API_CATEGORIES + System_Properties.CREDENTIALS_HEAL_AUEB;
        HashSet<String> allPublishers = new HashSet<String>();
        HashSet<String> allCategoriesSet = new HashSet<String>();
        String allCategories = Utils.getJSONDoc(urlCategories);
        JSONObject cats = (JSONObject) JSONValue.parse(allCategories);
        Set keys = cats.keySet();

        for (Object key : keys) {
            String k = key.toString();
            if (cats.get(k) instanceof JSONObject) {
                JSONObject jsonCategory = (JSONObject) cats.get(k);
                String catName = jsonCategory.get("cname").toString();
                String catID = k;
                allCategoriesSet.add(catName);
                String url = System_Properties.HEAL_API_URI + catID + "?" + System_Properties.CREDENTIALS_HEAL_AUEB;
                String docs = Utils.getJSONDoc(url);
                JSONObject obj_array = (JSONObject) JSONValue.parse(docs);
                System.out.println(jcl.keySet().size());
                Set array = obj_array.keySet();
                for (Object sobj : array) {
                    String sobj_key = sobj.toString();
                    if (obj_array.get(sobj_key) instanceof JSONObject) {
                        JSONObject obj = (JSONObject) obj_array.get(sobj_key);
                        String id = null;
                        String issn = "";
                        String title = "";
                        String url_j = "";
                        String pid = "";
                        String publisher = "";
                        String years_available = "";
                        try {
                            id = obj.get("ID").toString();
                        } catch (Exception e) {
                        }
                        try {

                            issn = obj.get("ISSN").toString();
                        } catch (Exception e) {
                        }
                        try {

                            title = obj.get("TITLE").toString();
                        } catch (Exception e) {
                        }

                        try {
                            url_j = obj.get("URL").toString();
                           
                        } catch (Exception e) {
                        }
                        try {

                            pid = obj.get("PID").toString();
                        } catch (Exception e) {
                        }
                        try {

                            publisher = obj.get("PUBLISHER").toString();
                        } catch (Exception e) {
                        }
                        try {

                            years_available = obj.get("YEARS_AVAILABLE").toString();
                        } catch (Exception e) {
                        }
                        allPublishers.add(publisher);
                        if (jcl.containsKey(id)) {
                            Journal j = jcl.get(id);
                            String cat = j.getClassification();
                            cat = cat + ", " + catName;
                            j.setClassification(cat);
                            jcl.put(id, j);
                        } else {
                            Journal j = new Journal(id, issn, title, catName, url_j, pid, publisher, years_available);
                            jcl.put(id, j);

                        }
                    }
                }

            }
        }

        PrintWriter p1 = new PrintWriter(System_Properties.CLASSIFICATIONS_PATH);
        for (String e : allCategoriesSet) {
            p1.println(e);
        }

        p1.close();

        PrintWriter p2 = new PrintWriter(System_Properties.PUBLISHERS_PATH);
        for (String e : allPublishers) {
            p2.println(e);
        }
        p2.close();

        for (Journal j : jcl.values()) {
            SolrInputDocument solrJournal = new SolrDocumentGenerator().getSolrJournal(j);
            Utils.commit(solrJournal);
        }

    }
    private static final Logger LOG = Logger.getLogger(HealHandler.class.getName());

}
