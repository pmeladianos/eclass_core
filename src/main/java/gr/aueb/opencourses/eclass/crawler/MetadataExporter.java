/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.params.System_Properties;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.*;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ListIterator;

/**
 *
 * @author Midas
 */
public class MetadataExporter {

    /**
     * A Map that will keep track of the schools and their faculties that the
     * crawler parsed and successfully committed to SOLR
     *
     */
    public static HashMap<String, HashSet<String>> scfc = new HashMap<String, HashSet<String>>();

    /**
     * A Map that will keep track of the faculties and their courses that the
     * crawler parsed and successfully committed to SOLR
     */
    public static HashMap<String, HashSet<String>> fccc = new HashMap<String, HashSet<String>>();
    private static String fieldtype = "type_t";
    private static String fieldcourse_name_t = "course_name_t";
    private static String fieldschool_name_t = "school_name_t";
    private static String fieldfaculty_t = "faculty_t";
    public static BasicHttpContext localContext = new BasicHttpContext();



    public MetadataExporter() {
        try {
            this.exportSchoolFaculty();
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //exportFacultyCourse();
        System.out.println("Done");

    }

    public  void exportSchoolFaculty() throws SolrServerException, IOException {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "12345"));

        BasicScheme basicAuth = new BasicScheme();
        localContext.setAttribute("preemptive-auth", basicAuth);

        httpclient.addRequestInterceptor(new PreemptiveAuthInterceptor(), 0);
        HttpSolrClient server = new HttpSolrClient(System_Properties.SOLR_URI, httpclient);
        SolrQuery solrSchoolQuery = new SolrQuery();
        String schoolQuery = fieldtype + ":school";
        solrSchoolQuery.setQuery(schoolQuery);

        QueryResponse schoolResponse = null;
        schoolResponse = server.query(solrSchoolQuery);

        SolrDocumentList schoolList = schoolResponse.getResults();
        ListIterator<SolrDocument> iter = schoolList.listIterator();
        while (iter.hasNext()) {
            SolrDocument doc = iter.next();
            String school = doc.get(fieldschool_name_t).toString();
            if (!scfc.containsKey(school)) {
                scfc.put(school, new HashSet<String>());
            }
            SolrQuery solrFacultyQuery = new SolrQuery();
            String facultyQuery = fieldtype + ":faculty AND " + fieldschool_name_t + ":" + "\"" + school + "\"";
            solrFacultyQuery.setQuery(facultyQuery);
            QueryResponse facultyResponse = null;
            facultyResponse = server.query(solrFacultyQuery);
            SolrDocumentList facultyList = facultyResponse.getResults();
            ListIterator<SolrDocument> iterf = facultyList.listIterator();
            while (iterf.hasNext()) {
                SolrDocument docF = iterf.next();
                String faculty = docF.get(fieldfaculty_t).toString();
                if (!fccc.containsKey(faculty)) {
                    fccc.put(faculty, new HashSet<String>());
                }
                SolrQuery solrCourseQuery = new SolrQuery();
                String courseQuery = fieldtype + ":course AND " + fieldschool_name_t + ":" + "\"" + school + "\""+" AND "+ fieldfaculty_t + ":" + "\"" + faculty + "\"";
                solrCourseQuery.setQuery(courseQuery);
                QueryResponse courseResponse = null;
                courseResponse = server.query(solrCourseQuery);
                SolrDocumentList courseList = courseResponse.getResults();
                ListIterator<SolrDocument> iterc = courseList.listIterator();
                while (iterc.hasNext()) {
                    SolrDocument docC = iterc.next();
                    String course = docC.get(fieldcourse_name_t).toString();
                    fccc.get(faculty).add(course);
                }

                scfc.get(school).add(faculty);
            }

        }
        Utils.printHashMapJSON("school", "faculty", System_Properties.SCHOOL_FACULTY_PATH, scfc);
        Utils.printHashMapJSON("faculty", "course", System_Properties.FACULTY_COURSE_PATH, fccc);
    }

    

    static class PreemptiveAuthInterceptor implements HttpRequestInterceptor {

        public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
            AuthState authState = (AuthState) context.getAttribute(HttpClientContext.TARGET_AUTH_STATE);

            // If no auth scheme avaialble yet, try to initialize it
            // preemptively
            if (authState.getAuthScheme() == null) {
                AuthScheme authScheme = (AuthScheme) localContext.getAttribute("preemptive-auth");
                CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(HttpClientContext.CREDS_PROVIDER);
                HttpHost targetHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
                if (authScheme != null) {
                    AuthScope sc = new AuthScope(targetHost.getHostName(), targetHost.getPort());
                    Credentials creds = credsProvider.getCredentials(sc);
                    if (creds == null) {
                        throw new HttpException("No credentials for preemptive authentication");
                    }
                    authState.update(authScheme, creds);
                }
            }

        }
    }
}
