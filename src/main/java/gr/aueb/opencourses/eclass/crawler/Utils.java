/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.eclass.crawler.handlers.FtpConnectionHandler;
import gr.aueb.opencourses.params.System_Properties;
import gr.aueb.opencourses.structures.Course;
import gr.aueb.opencourses.structures.EclassDocument;
import gr.aueb.opencourses.structures.EclassFolder;
import gr.aueb.opencourses.structures.solr.SolrDocumentGenerator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This is a class that contains several static helper functions used for
 * communication with the server communications,archiving a document and
 * traversing the folder structures.
 *
 * @author Midas
 */
public class Utils {

    public static final String greekStopwords="ΑΔΙΑΚΟΠΑ\n"+
            "ΑΙ\n"+
            "ΑΚΟΜΑ\n"+
            "ΑΚΟΜΗ\n"+
            "ΑΚΡΙΒΩΣ\n"+
            "ΑΛΗΘΕΙΑ\n"+
            "ΑΛΗΘΙΝΑ\n"+
            "ΑΛΛΑ\n"+
            "ΑΛΛΑΧΟΥ\n"+
            "ΑΛΛΕΣ\n"+
            "ΑΛΛΗ\n"+
            "ΑΛΛΗΝ\n"+
            "ΑΛΛΗΣ\n"+
            "ΑΛΛΙΩΣ\n"+
            "ΑΛΛΙΩΤΙΚΑ\n"+
            "ΑΛΛΟ\n"+
            "ΑΛΛΟΙ\n"+
            "ΑΛΛΟΙΩΣ\n"+
            "ΑΛΛΟΙΩΤΙΚΑ\n"+
            "ΑΛΛΟΝ\n"+
            "ΑΛΛΟΣ\n"+
            "ΑΛΛΟΤΕ\n"+
            "ΑΛΛΟΥ\n"+
            "ΑΛΛΟΥΣ\n"+
            "ΑΛΛΩΝ\n"+
            "ΑΜΑ\n"+
            "ΑΜΕΣΑ\n"+
            "ΑΜΕΣΩΣ\n"+
            "ΑΝ\n"+
            "ΑΝΑ\n"+
            "ΑΝΑΜΕΣΑ\n"+
            "ΑΝΑΜΕΤΑΞΥ\n"+
            "ΑΝΕΥ\n"+
            "ΑΝΤΙ\n"+
            "ΑΝΤΙΠΕΡΑ\n"+
            "ΑΝΤΙΣ\n"+
            "ΑΝΩ\n"+
            "ΑΝΩΤΕΡΩ\n"+
            "ΑΞΑΦΝΑ\n"+
            "ΑΠ\n"+
            "ΑΠΕΝΑΝΤΙ\n"+
            "ΑΠΟ\n"+
            "ΑΠΟΨΕ\n"+
            "ΑΡΑ\n"+
            "ΑΡΑΓΕ\n"+
            "ΑΡΓΑ\n"+
            "ΑΡΓΟΤΕΡΟ\n"+
            "ΑΡΙΣΤΕΡΑ\n"+
            "ΑΡΚΕΤΑ\n"+
            "ΑΡΧΙΚΑ\n"+
            "ΑΣ\n"+
            "ΑΥΡΙΟ\n"+
            "ΑΥΤΑ\n"+
            "ΑΥΤΕΣ\n"+
            "ΑΥΤΗ\n"+
            "ΑΥΤΗΝ\n"+
            "ΑΥΤΗΣ\n"+
            "ΑΥΤΟ\n"+
            "ΑΥΤΟΙ\n"+
            "ΑΥΤΟΝ\n"+
            "ΑΥΤΟΣ\n"+
            "ΑΥΤΟΥ\n"+
            "ΑΥΤΟΥΣ\n"+
            "ΑΥΤΩΝ\n"+
            "ΑΦΟΤΟΥ\n"+
            "ΑΦΟΥ\n"+
            "ΒΕΒΑΙΑ\n"+
            "ΒΕΒΑΙΟΤΑΤΑ\n"+
            "ΓΙ\n"+
            "ΓΙΑ\n"+
            "ΓΡΗΓΟΡΑ\n"+
            "ΓΥΡΩ\n"+
            "ΔΑ\n"+
            "ΔΕ\n"+
            "ΔΕΙΝΑ\n"+
            "ΔΕΝ\n"+
            "ΔΕΞΙΑ\n"+
            "ΔΗΘΕΝ\n"+
            "ΔΗΛΑΔΗ\n"+
            "ΔΙ\n"+
            "ΔΙΑ\n"+
            "ΔΙΑΡΚΩΣ\n"+
            "ΔΙΚΑ\n"+
            "ΔΙΚΟ\n"+
            "ΔΙΚΟΙ\n"+
            "ΔΙΚΟΣ\n"+
            "ΔΙΚΟΥ\n"+
            "ΔΙΚΟΥΣ\n"+
            "ΔΙΟΛΟΥ\n"+
            "ΔΙΠΛΑ\n"+
            "ΔΙΧΩΣ\n"+
            "ΕΑΝ\n"+
            "ΕΑΥΤΟ\n"+
            "ΕΑΥΤΟΝ\n"+
            "ΕΑΥΤΟΥ\n"+
            "ΕΑΥΤΟΥΣ\n"+
            "ΕΑΥΤΩΝ\n"+
            "ΕΓΚΑΙΡΑ\n"+
            "ΕΓΚΑΙΡΩΣ\n"+
            "ΕΓΩ\n"+
            "ΕΔΩ\n"+
            "ΕΙΔΕΜΗ\n"+
            "ΕΙΘΕ\n"+
            "ΕΙΜΑΙ\n"+
            "ΕΙΜΑΣΤΕ\n"+
            "ΕΙΝΑΙ\n"+
            "ΕΙΣ\n"+
            "ΕΙΣΑΙ\n"+
            "ΕΙΣΑΣΤΕ\n"+
            "ΕΙΣΤΕ\n"+
            "ΕΙΤΕ\n"+
            "ΕΙΧΑ\n"+
            "ΕΙΧΑΜΕ\n"+
            "ΕΙΧΑΝ\n"+
            "ΕΙΧΑΤΕ\n"+
            "ΕΙΧΕ\n"+
            "ΕΙΧΕΣ\n"+
            "ΕΚΑΣΤΑ\n"+
            "ΕΚΑΣΤΕΣ\n"+
            "ΕΚΑΣΤΗ\n"+
            "ΕΚΑΣΤΗΝ\n"+
            "ΕΚΑΣΤΗΣ\n"+
            "ΕΚΑΣΤΟ\n"+
            "ΕΚΑΣΤΟΙ\n"+
            "ΕΚΑΣΤΟΝ\n"+
            "ΕΚΑΣΤΟΣ\n"+
            "ΕΚΑΣΤΟΥ\n"+
            "ΕΚΑΣΤΟΥΣ\n"+
            "ΕΚΑΣΤΩΝ\n"+
            "ΕΚΕΙ\n"+
            "ΕΚΕΙΝΑ\n"+
            "ΕΚΕΙΝΕΣ\n"+
            "ΕΚΕΙΝΗ\n"+
            "ΕΚΕΙΝΗΝ\n"+
            "ΕΚΕΙΝΗΣ\n"+
            "ΕΚΕΙΝΟ\n"+
            "ΕΚΕΙΝΟΙ\n"+
            "ΕΚΕΙΝΟΝ\n"+
            "ΕΚΕΙΝΟΣ\n"+
            "ΕΚΕΙΝΟΥ\n"+
            "ΕΚΕΙΝΟΥΣ\n"+
            "ΕΚΕΙΝΩΝ\n"+
            "ΕΚΤΟΣ\n"+
            "ΕΜΑΣ\n"+
            "ΕΜΕΙΣ\n"+
            "ΕΜΕΝΑ\n"+
            "ΕΜΠΡΟΣ\n"+
            "ΕΝ\n"+
            "ΕΝΑ\n"+
            "ΕΝΑΝ\n"+
            "ΕΝΑΣ\n"+
            "ΕΝΟΣ\n"+
            "ΕΝΤΕΛΩΣ\n"+
            "ΕΝΤΟΣ\n"+
            "ΕΝΤΩΜΕΤΑΞΥ\n"+
            "ΕΝΩ\n"+
            "ΕΞ\n"+
            "ΕΞΑΦΝΑ\n"+
            "ΕΞΗΣ\n"+
            "ΕΞΙΣΟΥ\n"+
            "ΕΞΩ\n"+
            "ΕΠΑΝΩ\n"+
            "ΕΠΕΙΔΗ\n"+
            "ΕΠΕΙΤΑ\n"+
            "ΕΠΙ\n"+
            "ΕΠΙΣΗΣ\n"+
            "ΕΠΟΜΕΝΩΣ\n"+
            "ΕΣΑΣ\n"+
            "ΕΣΕΙΣ\n"+
            "ΕΣΕΝΑ\n"+
            "ΕΣΤΩ\n"+
            "ΕΣΥ\n"+
            "ΕΤΕΡΑ\n"+
            "ΕΤΕΡΑΙ\n"+
            "ΕΤΕΡΑΣ\n"+
            "ΕΤΕΡΕΣ\n"+
            "ΕΤΕΡΗ\n"+
            "ΕΤΕΡΗΣ\n"+
            "ΕΤΕΡΟ\n"+
            "ΕΤΕΡΟΙ\n"+
            "ΕΤΕΡΟΝ\n"+
            "ΕΤΕΡΟΣ\n"+
            "ΕΤΕΡΟΥ\n"+
            "ΕΤΕΡΟΥΣ\n"+
            "ΕΤΕΡΩΝ\n"+
            "ΕΤΟΥΤΑ\n"+
            "ΕΤΟΥΤΕΣ\n"+
            "ΕΤΟΥΤΗ\n"+
            "ΕΤΟΥΤΗΝ\n"+
            "ΕΤΟΥΤΗΣ\n"+
            "ΕΤΟΥΤΟ\n"+
            "ΕΤΟΥΤΟΙ\n"+
            "ΕΤΟΥΤΟΝ\n"+
            "ΕΤΟΥΤΟΣ\n"+
            "ΕΤΟΥΤΟΥ\n"+
            "ΕΤΟΥΤΟΥΣ\n"+
            "ΕΤΟΥΤΩΝ\n"+
            "ΕΤΣΙ\n"+
            "ΕΥΓΕ\n"+
            "ΕΥΘΥΣ\n"+
            "ΕΥΤΥΧΩΣ\n"+
            "ΕΦΕΞΗΣ\n"+
            "ΕΧΕΙ\n"+
            "ΕΧΕΙΣ\n"+
            "ΕΧΕΤΕ\n"+
            "ΕΧΘΕΣ\n"+
            "ΕΧΟΜΕ\n"+
            "ΕΧΟΥΜΕ\n"+
            "ΕΧΟΥΝ\n"+
            "ΕΧΤΕΣ\n"+
            "ΕΧΩ\n"+
            "ΕΩΣ\n"+
            "Η\n"+
            "ΗΔΗ\n"+
            "ΗΜΑΣΤΑΝ\n"+
            "ΗΜΑΣΤΕ\n"+
            "ΗΜΟΥΝ\n"+
            "ΗΣΑΣΤΑΝ\n"+
            "ΗΣΑΣΤΕ\n"+
            "ΗΣΟΥΝ\n"+
            "ΗΤΑΝ\n"+
            "ΗΤΑΝΕ\n"+
            "ΗΤΟΙ\n"+
            "ΗΤΤΟΝ\n"+
            "ΘΑ\n"+
            "Ι\n"+
            "ΙΔΙΑ\n"+
            "ΙΔΙΑΝ\n"+
            "ΙΔΙΑΣ\n"+
            "ΙΔΙΕΣ\n"+
            "ΙΔΙΟ\n"+
            "ΙΔΙΟΙ\n"+
            "ΙΔΙΟΝ\n"+
            "ΙΔΙΟΣ\n"+
            "ΙΔΙΟΥ\n"+
            "ΙΔΙΟΥΣ\n"+
            "ΙΔΙΩΝ\n"+
            "ΙΔΙΩΣ\n"+
            "ΙΙ\n"+
            "ΙΙΙ\n"+
            "ΙΣΑΜΕ\n"+
            "ΙΣΙΑ\n"+
            "ΙΣΩΣ\n"+
            "ΚΑΘΕ\n"+
            "ΚΑΘΕΜΙΑ\n"+
            "ΚΑΘΕΜΙΑΣ\n"+
            "ΚΑΘΕΝΑ\n"+
            "ΚΑΘΕΝΑΣ\n"+
            "ΚΑΘΕΝΟΣ\n"+
            "ΚΑΘΕΤΙ\n"+
            "ΚΑΘΟΛΟΥ\n"+
            "ΚΑΘΩΣ\n"+
            "ΚΑΙ\n"+
            "ΚΑΚΑ\n"+
            "ΚΑΚΩΣ\n"+
            "ΚΑΛΑ\n"+
            "ΚΑΛΩΣ\n"+
            "ΚΑΜΙΑ\n"+
            "ΚΑΜΙΑΝ\n"+
            "ΚΑΜΙΑΣ\n"+
            "ΚΑΜΠΟΣΑ\n"+
            "ΚΑΜΠΟΣΕΣ\n"+
            "ΚΑΜΠΟΣΗ\n"+
            "ΚΑΜΠΟΣΗΝ\n"+
            "ΚΑΜΠΟΣΗΣ\n"+
            "ΚΑΜΠΟΣΟ\n"+
            "ΚΑΜΠΟΣΟΙ\n"+
            "ΚΑΜΠΟΣΟΝ\n"+
            "ΚΑΜΠΟΣΟΣ\n"+
            "ΚΑΜΠΟΣΟΥ\n"+
            "ΚΑΜΠΟΣΟΥΣ\n"+
            "ΚΑΜΠΟΣΩΝ\n"+
            "ΚΑΝΕΙΣ\n"+
            "ΚΑΝΕΝ\n"+
            "ΚΑΝΕΝΑ\n"+
            "ΚΑΝΕΝΑΝ\n"+
            "ΚΑΝΕΝΑΣ\n"+
            "ΚΑΝΕΝΟΣ\n"+
            "ΚΑΠΟΙΑ\n"+
            "ΚΑΠΟΙΑΝ\n"+
            "ΚΑΠΟΙΑΣ\n"+
            "ΚΑΠΟΙΕΣ\n"+
            "ΚΑΠΟΙΟ\n"+
            "ΚΑΠΟΙΟΙ\n"+
            "ΚΑΠΟΙΟΝ\n"+
            "ΚΑΠΟΙΟΣ\n"+
            "ΚΑΠΟΙΟΥ\n"+
            "ΚΑΠΟΙΟΥΣ\n"+
            "ΚΑΠΟΙΩΝ\n"+
            "ΚΑΠΟΤΕ\n"+
            "ΚΑΠΟΥ\n"+
            "ΚΑΠΩΣ\n"+
            "ΚΑΤ\n"+
            "ΚΑΤΑ\n"+
            "ΚΑΤΙ\n"+
            "ΚΑΤΙΤΙ\n"+
            "ΚΑΤΟΠΙΝ\n"+
            "ΚΑΤΩ\n"+
            "ΚΙΟΛΑΣ\n"+
            "ΚΛΠ\n"+
            "ΚΟΝΤΑ\n"+
            "ΚΤΛ\n"+
            "ΚΥΡΙΩΣ\n"+
            "ΛΙΓΑΚΙ\n"+
            "ΛΙΓΟ\n"+
            "ΛΙΓΩΤΕΡΟ\n"+
            "ΛΟΓΩ\n"+
            "ΛΟΙΠΑ\n"+
            "ΛΟΙΠΟΝ\n"+
            "ΜΑ\n"+
            "ΜΑΖΙ\n"+
            "ΜΑΚΑΡΙ\n"+
            "ΜΑΚΡΥΑ\n"+
            "ΜΑΛΙΣΤΑ\n"+
            "ΜΑΛΛΟΝ\n"+
            "ΜΑΣ\n"+
            "ΜΕ\n"+
            "ΜΕΘΑΥΡΙΟ\n"+
            "ΜΕΙΟΝ\n"+
            "ΜΕΛΕΙ\n"+
            "ΜΕΛΛΕΤΑΙ\n"+
            "ΜΕΜΙΑΣ\n"+
            "ΜΕΝ\n"+
            "ΜΕΡΙΚΑ\n"+
            "ΜΕΡΙΚΕΣ\n"+
            "ΜΕΡΙΚΟΙ\n"+
            "ΜΕΡΙΚΟΥΣ\n"+
            "ΜΕΡΙΚΩΝ\n"+
            "ΜΕΣΑ\n"+
            "ΜΕΤ\n"+
            "ΜΕΤΑ\n"+
            "ΜΕΤΑΞΥ\n"+
            "ΜΕΧΡΙ\n"+
            "ΜΗ\n"+
            "ΜΗΔΕ\n"+
            "ΜΗΝ\n"+
            "ΜΗΠΩΣ\n"+
            "ΜΗΤΕ\n"+
            "ΜΙΑ\n"+
            "ΜΙΑΝ\n"+
            "ΜΙΑΣ\n"+
            "ΜΟΛΙΣ\n"+
            "ΜΟΛΟΝΟΤΙ\n"+
            "ΜΟΝΑΧΑ\n"+
            "ΜΟΝΕΣ\n"+
            "ΜΟΝΗ\n"+
            "ΜΟΝΗΝ\n"+
            "ΜΟΝΗΣ\n"+
            "ΜΟΝΟ\n"+
            "ΜΟΝΟΙ\n"+
            "ΜΟΝΟΜΙΑΣ\n"+
            "ΜΟΝΟΣ\n"+
            "ΜΟΝΟΥ\n"+
            "ΜΟΝΟΥΣ\n"+
            "ΜΟΝΩΝ\n"+
            "ΜΟΥ\n"+
            "ΜΠΟΡΕΙ\n"+
            "ΜΠΟΡΟΥΝ\n"+
            "ΜΠΡΑΒΟ\n"+
            "ΜΠΡΟΣ\n"+
            "ΝΑ\n"+
            "ΝΑΙ\n"+
            "ΝΩΡΙΣ\n"+
            "ΞΑΝΑ\n"+
            "ΞΑΦΝΙΚΑ\n"+
            "Ο\n"+
            "ΟΙ\n"+
            "ΟΛΑ\n"+
            "ΟΛΕΣ\n"+
            "ΟΛΗ\n"+
            "ΟΛΗΝ\n"+
            "ΟΛΗΣ\n"+
            "ΟΛΟ\n"+
            "ΟΛΟΓΥΡΑ\n"+
            "ΟΛΟΙ\n"+
            "ΟΛΟΝ\n"+
            "ΟΛΟΝΕΝ\n"+
            "ΟΛΟΣ\n"+
            "ΟΛΟΤΕΛΑ\n"+
            "ΟΛΟΥ\n"+
            "ΟΛΟΥΣ\n"+
            "ΟΛΩΝ\n"+
            "ΟΛΩΣ\n"+
            "ΟΛΩΣΔΙΟΛΟΥ\n"+
            "ΟΜΩΣ\n"+
            "ΟΠΟΙΑ\n"+
            "ΟΠΟΙΑΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΑΝ\n"+
            "ΟΠΟΙΑΝΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΑΣ\n"+
            "ΟΠΟΙΑΣΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΕΣ\n"+
            "ΟΠΟΙΕΣΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΟ\n"+
            "ΟΠΟΙΟΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΟΙ\n"+
            "ΟΠΟΙΟΝ\n"+
            "ΟΠΟΙΟΝΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΟΣ\n"+
            "ΟΠΟΙΟΣΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΟΥ\n"+
            "ΟΠΟΙΟΥΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΟΥΣ\n"+
            "ΟΠΟΙΟΥΣΔΗΠΟΤΕ\n"+
            "ΟΠΟΙΩΝ\n"+
            "ΟΠΟΙΩΝΔΗΠΟΤΕ\n"+
            "ΟΠΟΤΕ\n"+
            "ΟΠΟΤΕΔΗΠΟΤΕ\n"+
            "ΟΠΟΥ\n"+
            "ΟΠΟΥΔΗΠΟΤΕ\n"+
            "ΟΠΩΣ\n"+
            "ΟΡΙΣΜΕΝΑ\n"+
            "ΟΡΙΣΜΕΝΕΣ\n"+
            "ΟΡΙΣΜΕΝΩΝ\n"+
            "ΟΡΙΣΜΕΝΩΣ\n"+
            "ΟΣΑ\n"+
            "ΟΣΑΔΗΠΟΤΕ\n"+
            "ΟΣΕΣ\n"+
            "ΟΣΕΣΔΗΠΟΤΕ\n"+
            "ΟΣΗ\n"+
            "ΟΣΗΔΗΠΟΤΕ\n"+
            "ΟΣΗΝ\n"+
            "ΟΣΗΝΔΗΠΟΤΕ\n"+
            "ΟΣΗΣ\n"+
            "ΟΣΗΣΔΗΠΟΤΕ\n"+
            "ΟΣΟ\n"+
            "ΟΣΟΔΗΠΟΤΕ\n"+
            "ΟΣΟΙ\n"+
            "ΟΣΟΙΔΗΠΟΤΕ\n"+
            "ΟΣΟΝ\n"+
            "ΟΣΟΝΔΗΠΟΤΕ\n"+
            "ΟΣΟΣ\n"+
            "ΟΣΟΣΔΗΠΟΤΕ\n"+
            "ΟΣΟΥ\n"+
            "ΟΣΟΥΔΗΠΟΤΕ\n"+
            "ΟΣΟΥΣ\n"+
            "ΟΣΟΥΣΔΗΠΟΤΕ\n"+
            "ΟΣΩΝ\n"+
            "ΟΣΩΝΔΗΠΟΤΕ\n"+
            "ΟΤΑΝ\n"+
            "ΟΤΙ\n"+
            "ΟΤΙΔΗΠΟΤΕ\n"+
            "ΟΤΟΥ\n"+
            "ΟΥ\n"+
            "ΟΥΔΕ\n"+
            "ΟΥΤΕ\n"+
            "ΟΧΙ\n"+
            "ΠΑΛΙ\n"+
            "ΠΑΝΤΟΤΕ\n"+
            "ΠΑΝΤΟΥ\n"+
            "ΠΑΝΤΩΣ\n"+
            "ΠΑΡΑ\n"+
            "ΠΕΡΑ\n"+
            "ΠΕΡΙ\n"+
            "ΠΕΡΙΠΟΥ\n"+
            "ΠΕΡΙΣΣΟΤΕΡΟ\n"+
            "ΠΕΡΣΙ\n"+
            "ΠΕΡΥΣΙ\n"+
            "ΠΙΑ\n"+
            "ΠΙΘΑΝΟΝ\n"+
            "ΠΙΟ\n"+
            "ΠΙΣΩ\n"+
            "ΠΛΑΙ\n"+
            "ΠΛΕΟΝ\n"+
            "ΠΛΗΝ\n"+
            "ΠΟΙΑ\n"+
            "ΠΟΙΑΝ\n"+
            "ΠΟΙΑΣ\n"+
            "ΠΟΙΕΣ\n"+
            "ΠΟΙΟ\n"+
            "ΠΟΙΟΙ\n"+
            "ΠΟΙΟΝ\n"+
            "ΠΟΙΟΣ\n"+
            "ΠΟΙΟΥ\n"+
            "ΠΟΙΟΥΣ\n"+
            "ΠΟΙΩΝ\n"+
            "ΠΟΛΥ\n"+
            "ΠΟΣΕΣ\n"+
            "ΠΟΣΗ\n"+
            "ΠΟΣΗΝ\n"+
            "ΠΟΣΗΣ\n"+
            "ΠΟΣΟΙ\n"+
            "ΠΟΣΟΣ\n"+
            "ΠΟΣΟΥΣ\n"+
            "ΠΟΤΕ\n"+
            "ΠΟΥ\n"+
            "ΠΟΥΘΕ\n"+
            "ΠΟΥΘΕΝΑ\n"+
            "ΠΡΕΠΕΙ\n"+
            "ΠΡΙΝ\n"+
            "ΠΡΟ\n"+
            "ΠΡΟΚΕΙΜΕΝΟΥ\n"+
            "ΠΡΟΚΕΙΤΑΙ\n"+
            "ΠΡΟΠΕΡΣΙ\n"+
            "ΠΡΟΣ\n"+
            "ΠΡΟΤΟΥ\n"+
            "ΠΡΟΧΘΕΣ\n"+
            "ΠΡΟΧΤΕΣ\n"+
            "ΠΡΩΤΥΤΕΡΑ\n"+
            "ΠΩΣ\n"+
            "ΣΑΝ\n"+
            "ΣΑΣ\n"+
            "ΣΕ\n"+
            "ΣΕΙΣ\n"+
            "ΣΗΜΕΡΑ\n"+
            "ΣΙΓΑ\n"+
            "ΣΟΥ\n"+
            "ΣΤΑ\n"+
            "ΣΤΗ\n"+
            "ΣΤΗΝ\n"+
            "ΣΤΗΣ\n"+
            "ΣΤΙΣ\n"+
            "ΣΤΟ\n"+
            "ΣΤΟΝ\n"+
            "ΣΤΟΥ\n"+
            "ΣΤΟΥΣ\n"+
            "ΣΤΩΝ\n"+
            "ΣΥΓΧΡΟΝΩΣ\n"+
            "ΣΥΝ\n"+
            "ΣΥΝΑΜΑ\n"+
            "ΣΥΝΕΠΩΣ\n"+
            "ΣΥΝΗΘΩΣ\n"+
            "ΣΥΧΝΑ\n"+
            "ΣΥΧΝΑΣ\n"+
            "ΣΥΧΝΕΣ\n"+
            "ΣΥΧΝΗ\n"+
            "ΣΥΧΝΗΝ\n"+
            "ΣΥΧΝΗΣ\n"+
            "ΣΥΧΝΟ\n"+
            "ΣΥΧΝΟΙ\n"+
            "ΣΥΧΝΟΝ\n"+
            "ΣΥΧΝΟΣ\n"+
            "ΣΥΧΝΟΥ\n"+
            "ΣΥΧΝΟΥ\n"+
            "ΣΥΧΝΟΥΣ\n"+
            "ΣΥΧΝΩΝ\n"+
            "ΣΥΧΝΩΣ\n"+
            "ΣΧΕΔΟΝ\n"+
            "ΣΩΣΤΑ\n"+
            "ΤΑ\n"+
            "ΤΑΔΕ\n"+
            "ΤΑΥΤΑ\n"+
            "ΤΑΥΤΕΣ\n"+
            "ΤΑΥΤΗ\n"+
            "ΤΑΥΤΗΝ\n"+
            "ΤΑΥΤΗΣ\n"+
            "ΤΑΥΤΟ,ΤΑΥΤΟΝ\n"+
            "ΤΑΥΤΟΣ\n"+
            "ΤΑΥΤΟΥ\n"+
            "ΤΑΥΤΩΝ\n"+
            "ΤΑΧΑ\n"+
            "ΤΑΧΑΤΕ\n"+
            "ΤΕΛΙΚΑ\n"+
            "ΤΕΛΙΚΩΣ\n"+
            "ΤΕΣ\n"+
            "ΤΕΤΟΙΑ\n"+
            "ΤΕΤΟΙΑΝ\n"+
            "ΤΕΤΟΙΑΣ\n"+
            "ΤΕΤΟΙΕΣ\n"+
            "ΤΕΤΟΙΟ\n"+
            "ΤΕΤΟΙΟΙ\n"+
            "ΤΕΤΟΙΟΝ\n"+
            "ΤΕΤΟΙΟΣ\n"+
            "ΤΕΤΟΙΟΥ\n"+
            "ΤΕΤΟΙΟΥΣ\n"+
            "ΤΕΤΟΙΩΝ\n"+
            "ΤΗ\n"+
            "ΤΗΝ\n"+
            "ΤΗΣ\n"+
            "ΤΙ\n"+
            "ΤΙΠΟΤΑ\n"+
            "ΤΙΠΟΤΕ\n"+
            "ΤΙΣ\n"+
            "ΤΟ\n"+
            "ΤΟΙ\n"+
            "ΤΟΝ\n"+
            "ΤΟΣ\n"+
            "ΤΟΣΑ\n"+
            "ΤΟΣΕΣ\n"+
            "ΤΟΣΗ\n"+
            "ΤΟΣΗΝ\n"+
            "ΤΟΣΗΣ\n"+
            "ΤΟΣΟ\n"+
            "ΤΟΣΟΙ\n"+
            "ΤΟΣΟΝ\n"+
            "ΤΟΣΟΣ\n"+
            "ΤΟΣΟΥ\n"+
            "ΤΟΣΟΥΣ\n"+
            "ΤΟΣΩΝ\n"+
            "ΤΟΤΕ\n"+
            "ΤΟΥ\n"+
            "ΤΟΥΛΑΧΙΣΤΟ\n"+
            "ΤΟΥΛΑΧΙΣΤΟΝ\n"+
            "ΤΟΥΣ\n"+
            "ΤΟΥΤΑ\n"+
            "ΤΟΥΤΕΣ\n"+
            "ΤΟΥΤΗ\n"+
            "ΤΟΥΤΗΝ\n"+
            "ΤΟΥΤΗΣ\n"+
            "ΤΟΥΤΟ\n"+
            "ΤΟΥΤΟΙ\n"+
            "ΤΟΥΤΟΙΣ\n"+
            "ΤΟΥΤΟΝ\n"+
            "ΤΟΥΤΟΣ\n"+
            "ΤΟΥΤΟΥ\n"+
            "ΤΟΥΤΟΥΣ\n"+
            "ΤΟΥΤΩΝ\n"+
            "ΤΥΧΟΝ\n"+
            "ΤΩΝ\n"+
            "ΤΩΡΑ\n"+
            "ΥΠ\n"+
            "ΥΠΕΡ\n"+
            "ΥΠΟ\n"+
            "ΥΠΟΨΗ\n"+
            "ΥΠΟΨΙΝ\n"+
            "ΥΣΤΕΡΑ\n"+
            "ΦΕΤΟΣ\n"+
            "ΧΑΜΗΛΑ\n"+
            "ΧΘΕΣ\n"+
            "ΧΤΕΣ\n"+
            "ΧΩΡΙΣ\n"+
            "ΧΩΡΙΣΤΑ\n"+
            "ΨΗΛΑ\n"+
            "Ω\n"+
            "ΩΡΑΙΑ\n"+
            "ΩΣ\n"+
            "ΩΣΑΝ\n"+
            "ΩΣΟΤΟΥ\n"+
            "ΩΣΠΟΥ\n"+
            "ΩΣΤΕ\n"+
            "ΩΣΤΟΣΟ\n"+
            "ΥΣΤΕΡ\n"+
            "ΜΠΟΡ\n"+
            "ΧΡΗΣΙΜΟΠΟΙ\n"+
            "ΩΧ";


    public static String parseToPlainText(String link) throws IOException, TikaException {
        Tika tika = new Tika();
        String filecontent = tika.parseToString(new URL(link));
        return filecontent;
    }

    public static String parseToPlainText(File f) throws IOException, TikaException {
        Tika tika = new Tika();
        String filecontent = tika.parseToString(f);
        return filecontent;
    }

    public static boolean commit(SolrInputDocument doc) {
        if (System_Properties.COMMIT) {
            try {

                Thread.sleep(System_Properties.HTTP_DELAY);
                UpdateRequest req = new UpdateRequest();
                req.setAction(UpdateRequest.ACTION.COMMIT, false, false);
                req.add(doc);

                UpdateResponse add = Connector.server.add(doc);
                UpdateResponse commit = Connector.server.commit();
                UpdateResponse rsp = req.process(Connector.server);
                return true;
            } catch (Exception ex) {

                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
                return false;

            }
        } else {
            return true;
        }
    }

    //isArchived does not reflect a boolean variable
    public static String archive(String link, FtpConnectionHandler ftp) throws IOException, MalformedURLException, InterruptedException {
        String isArchived = null;
        if (!System_Properties.USE_FTP) {
            isArchived = archiveDocument(link);
        } else {
            isArchived = archiveDocument(link, ftp);
        }
        return isArchived;
    }

    private static String archiveDocument(String link) {
        try {
            URL website = new URL(link);
            ReadableByteChannel rbc = Channels.newChannel(website.openStream());
            String ext = link.split("\\.")[link.split("\\.").length - 1];

            String fileServerlink = link.replaceFirst("https://", "").replaceFirst("http://", "");

            String rootFolder = fileServerlink.split(".gr/modules/document/file.php/")[0];

            String name = fileServerlink.split(".gr/modules/document/file.php/")[1];
            String courseFolder = name.split("/")[0];
            name = name.split("/")[name.split("/").length - 1];
            String relativePath = rootFolder + "/" + courseFolder + "/" + name.replaceAll("%", "");
            if (relativePath.length() > 253) {
                relativePath = relativePath.substring(0, 252);
            }
            File theDir = new File(System_Properties.FILESERVERPATH + rootFolder + "/" + courseFolder);

            if (!theDir.exists()) {

                try {
                    theDir.mkdirs();
                } catch (SecurityException se) {
                    Logger.getLogger(EclassHandler.class.getName()).log(Level.SEVERE, null, se);
                }

            }

            FileOutputStream fos = new FileOutputStream(System_Properties.FILESERVERPATH + relativePath);
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            return relativePath;

        } catch (Exception e) {
            Logger.getLogger(EclassHandler.class.getName()).log(Level.SEVERE, null, e);
        }

        return null;
    }

    private static String archiveDocument(String link, FtpConnectionHandler ftp) throws MalformedURLException, IOException, InterruptedException {

        String fileServerlink = link.replaceFirst("https://", "").replaceFirst("http://", "");

        String rootFolder = fileServerlink.split(".gr/modules/document/file.php/")[0];

        String name = fileServerlink.split(".gr/modules/document/file.php/")[1];
        String courseFolder = name.split("/")[0];
        name = name.split("/")[name.split("/").length - 1];
        if (name.length() > 150) {
            name = name.substring(name.length() - 25, name.length());
        }
        String relativePath = rootFolder + "/" + courseFolder + "/" + name.replaceAll("%", "");//+".gz";

        File theDir = new File(System_Properties.FILESERVERPATH + rootFolder + "/" + courseFolder);

        if (!theDir.exists()) {

            try {
                theDir.mkdirs();
            } catch (SecurityException se) {
                Logger.getLogger(EclassHandler.class.getName()).log(Level.SEVERE, null, se);
            }

        }
        getHttpAndSavetoFolder(link, System_Properties.FILESERVERPATH + relativePath);
        String localPathFile = System_Properties.FILESERVERPATH + "/" + relativePath;
        String fileServerPath = ftp.commitLocal2FileServer(rootFolder, courseFolder, relativePath);
        Files.delete(Paths.get(localPathFile));
        return fileServerPath;

    }

    public static List<EclassDocument> getAllFolderDocuments(List<EclassFolder> folders) {
        List<EclassFolder> temp_folders = new ArrayList<EclassFolder>();
        List<EclassDocument> temp_docs = new ArrayList<EclassDocument>();
        for (EclassFolder folder : folders) {
            temp_docs.addAll(folder.getDocuments());
            temp_folders.addAll(folder.getFolders());
        }
        if (!temp_folders.isEmpty()) {
            temp_docs.addAll(getAllFolderDocuments(temp_folders));
        }
        return temp_docs;
    }

    public static Boolean commitCourseInfoSolr(String eclass_school, String faculty, Course c) {
        System.out.println("Commiting info to Solr");
        List<EclassDocument> docs = new ArrayList<EclassDocument>();
        docs.addAll(c.getDocuments());
        docs.addAll(Utils.getAllFolderDocuments(c.getFolders()));

        for (EclassDocument edoc : docs) {

            SolrInputDocument doc = new SolrDocumentGenerator().getSolrDocument(c, edoc);

            Utils.commit(doc);

        }
        return true;

    }

    public static Document getJsoupDoc(String url) throws InterruptedException, IOException {

        Thread.sleep(System_Properties.HTTP_DELAY);
        String html = getHtml(getNewHttpClient(), url);
        Document doc = Jsoup.parse(html);
        // System.out.println(doc.text());
        return doc;
    }

    public static void getHttpAndSavetoFolder(String url, String filePath) throws InterruptedException, IOException {
        Thread.sleep(System_Properties.HTTP_DELAY);
        HttpClient client = getNewHttpClient();
        HttpGet httpget = new HttpGet(url);
        HttpResponse response = client.execute(httpget);
        HttpEntity entity = response.getEntity();
        FileOutputStream fos;
       
        try (InputStream is = entity.getContent()) {
            fos = new FileOutputStream(new File(filePath));
            //GZIPOutputStream gzos = new GZIPOutputStream(fos);
            int inByte;
            while ((inByte = is.read()) != -1) {
                fos.write(inByte);
            }
            //gzos.finish();
            //gzos.close();
            fos.close();
        }

    }

    /**
     * Sets up a client that will accept any certificate.
     *
     * DO NOT USE IN PRODUCTION ENVIRONMENT!
     *
     * @return the insecure client
     */
    private static HttpClient getNewHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(MySSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

    private static String getHtml(HttpClient httpClient, String url) {
        StringBuilder sb = new StringBuilder();
        try {
            HttpGet getRequest = new HttpGet(url);
            getRequest.addHeader("accept", "text/plain");

            HttpResponse response = httpClient.execute(getRequest);

            String output;
            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent()), "UTF8"));
            while ((output = br.readLine()) != null) {
                sb.append(output);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        httpClient.getConnectionManager().shutdown();
        return sb.toString();
    }

    public static void printHashMapJSON(String keyName, String valueName, String path, HashMap<String, HashSet<String>> map) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintWriter writer = new PrintWriter(path, "UTF-8")) {
            writer.println("[");
            int lines = 0;
            int totalLines = map.entrySet().size();
            for (Map.Entry<String, HashSet<String>> entry : map.entrySet()) {
                lines++;
                JSONObject obj = new JSONObject();
                obj.put(keyName, entry.getKey());
                JSONArray list = new JSONArray();

                for (String e : entry.getValue()) {
                    list.add(e);
                }
                obj.put(valueName, list);
                writer.println(obj.toJSONString());
                if (lines < totalLines) {
                    writer.println(",");
                }

            }
            writer.println("]");
        }

    }

    public static String getJSONDoc(String url) throws InterruptedException {
        Thread.sleep(System_Properties.HTTP_DELAY);
        String json = getHtml(getNewHttpClient(), url);
        return json;
    }

}
