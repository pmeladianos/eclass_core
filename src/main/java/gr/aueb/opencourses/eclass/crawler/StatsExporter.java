/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import gr.aueb.opencourses.params.System_Properties;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.*;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

import static gr.aueb.opencourses.eclass.crawler.MetadataExporter.localContext;

/**
 *
 * @author pmeladianos
 */
public class StatsExporter {

    public static void main(String... args) throws SolrServerException, IOException {

        SolrDocumentList list = getAllResuts();

        Map<String, Integer> teachers = getTopRanks(list, "teacher_name_t");
        printCSV(teachers, "teachers");

    }

    private static SolrDocumentList getAllResuts() throws SolrServerException, IOException {
        DefaultHttpClient httpclient = new DefaultHttpClient();
        //httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("admin", "12345"));
        httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(System_Properties.SOLRADMINUNAME, System_Properties.SOLRADMINPASS));
        BasicScheme basicAuth = new BasicScheme();
        localContext.setAttribute("preemptive-auth", basicAuth);

        httpclient.addRequestInterceptor(new MetadataExporter.PreemptiveAuthInterceptor(), 0);
        HttpSolrClient server = new HttpSolrClient(System_Properties.SOLR_URI, httpclient);
        SolrQuery solrQuery = new SolrQuery();
        String query = "type_t:school";
        solrQuery.setQuery(query);
        solrQuery.setRows(1000);
        QueryResponse schoolResponse = null;
        schoolResponse = server.query(solrQuery);

        SolrDocumentList schoolList = schoolResponse.getResults();
        for (SolrDocument school:schoolList ){
            SolrQuery solrQuery_s = new SolrQuery();
            String query_s = "type_t:document AND school_name_t:("+school.get("school_name_t")+")";
            solrQuery_s.setQuery(query_s);
            solrQuery_s.setRows(100000);
            QueryResponse schoolResponse_s = null;
            schoolResponse_s = server.query(solrQuery_s);
            System.out.println(school.get("school_name_t").toString()+" : "+schoolResponse_s.getResults().size());
        }
        return schoolList;

    }

    private static void printCSV(Map<String, Integer> map, String field) throws FileNotFoundException, UnsupportedEncodingException {
        PrintWriter t = new PrintWriter(System_Properties.STATS_EXPORT_PATH + "/top" + field + ".csv", "UTF-8");

        for (Map.Entry<String, Integer> e : map.entrySet()) {
            t.println(e.getKey() + "," + e.getValue());
        }
        t.close();

    }

    private static Map<String, Integer> getTopRanks(SolrDocumentList list, String field) {
        HashMap<String, Integer> unsorted = new HashMap<String, Integer>();
        list.forEach(d -> {
            String field_value = d.get(field).toString();
            if (!(field_value.equals("-") || field_value.isEmpty())) {
                if (unsorted.containsKey(field_value)) {
                    unsorted.put(field_value, unsorted.get(field_value) + 1);
                } else {
                    unsorted.put(field_value, 1);
                }
            }
        });
        Map<String, Integer> sortedMap = sortByComparator(unsorted);
        return sortedMap;
    }

    static class PreemptiveAuthInterceptor implements HttpRequestInterceptor {

        public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
            AuthState authState = (AuthState) context.getAttribute(HttpClientContext.TARGET_AUTH_STATE);

            // If no auth scheme avaialble yet, try to initialize it
            // preemptively
            if (authState.getAuthScheme() == null) {
                AuthScheme authScheme = (AuthScheme) localContext.getAttribute("preemptive-auth");
                CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(HttpClientContext.CREDS_PROVIDER);
                HttpHost targetHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
                if (authScheme != null) {
                    AuthScope sc = new AuthScope(targetHost.getHostName(), targetHost.getPort());
                    Credentials creds = credsProvider.getCredentials(sc);
                    if (creds == null) {
                        throw new HttpException("No credentials for preemptive authentication");
                    }
                    authState.update(authScheme, creds);
                }
            }

        }
    }

    private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap) {

        // Convert Map to List
        List<Map.Entry<String, Integer>> list
                = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sort list with comparator, to compare the Map values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // Convert sorted map back to a Map
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Iterator<Map.Entry<String, Integer>> it = list.iterator(); it.hasNext();) {
            Map.Entry<String, Integer> entry = it.next();
            sortedMap.put(entry.getKey(), entry.getValue());

        }
        return sortedMap;
    }

}
