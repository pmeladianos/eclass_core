/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.eclass.crawler;

import com.jcraft.jsch.JSchException;
import gr.aueb.opencourses.eclass.crawler.handlers.FtpConnectionHandler;
import gr.aueb.opencourses.params.System_Properties;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.*;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpCoreContext;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.tika.exception.TikaException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class initializes the connection to the SOLR Server and calls a Parser
 * for every seed When the parsing is complete it writes the collected resources
 * to the database. All the methods require no arguments. The system is
 * parametrized using an external properties file(crawler_properties.prop) for
 * convenience.
 *
 * @author Midas
 */
public class Connector {

    /**
     * The Solr Server where the crawler will commit the data
     */
    public static HttpSolrClient server;
    public static FtpConnectionHandler ftp;
    /**
     * The context that will be used for the communication with the Jetty server
     * of SOLR
     */
    public static BasicHttpContext localContext = new BasicHttpContext();

    /**
     * A Map that will keep track of the schools and their faculties that the
     * crawler parsed and successfully committed to SOLR
     *
     */
    //public static Map<String, HashSet<String>> scfc = new HashMap<String, HashSet<String>>();
    /**
     * A Map that will keep track of the faculties and their courses that the
     * crawler parsed and successfully committed to SOLR
     */
    //public static Map<String, HashSet<String>> fccc = new HashMap<String, HashSet<String>>();
    //public static PrintWriter obj_writer;
    // public static DBCollection collection ;
    private static final Logger log = Logger.getLogger(Connector.class.getName());

    /**
     * This is the entry point of the collector
     *
     * @author Midas
     * @param args
     * @throws java.io.IOException
     * @throws java.net.MalformedURLException
     * @throws org.apache.tika.exception.TikaException
     * @throws java.lang.InterruptedException
     */
    public static void main(String... args) throws IOException, MalformedURLException, TikaException, InterruptedException, JSchException {
        System_Properties p = new System_Properties();
        p.init();

        //Mongo mongo = new Mongo(System_Properties.OBJ_DB, System_Properties.OBJ_DB_PORT);
        //DB db = mongo.getDB("yourdb");
        //collection = db.getCollection("dummyColl");
        List<String> eclass_seed_list = p.getECLASS_SEED_LIST();
        DefaultHttpClient httpclient = new DefaultHttpClient();
        httpclient.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(System_Properties.SOLRADMINUNAME, System_Properties.SOLRADMINPASS));
        BasicScheme basicAuth = new BasicScheme();
        localContext.setAttribute("preemptive-auth", basicAuth);
        httpclient.addRequestInterceptor(new PreemptiveAuthInterceptor(), 0);
        server = new HttpSolrClient(System_Properties.SOLR_URI, httpclient);
        ftp = new FtpConnectionHandler();
        ftp.setupConnection();
        switch (System_Properties.TARGET_CRAWL) {
            case "HEAL_LINK":
                System.out.println("Indexing Heal_link ");
                HealHandler healLinks = new HealHandler();
                break;
            case "CONCEPTS":
                ConceptHandler concepts = new ConceptHandler();
                break;
            default:
                for (String seed : eclass_seed_list) {
                    try {
                        System.out.println("Indexing eClass " + seed);
                        EclassHandler parser = new EclassHandler(seed);
                    } catch (Exception e) {
                        log.log(Level.SEVERE, e.toString());
                        e.printStackTrace();
                        continue;
                    }
                }
                break;
        }
        //MetadataExporter meta=new MetadataExporter();
        ftp.disconnect();
        System.exit(0);

    }

    /**
     * This class contains methods that are related to the client-server
     * connection and authentication.
     *
     *
     */
    static class PreemptiveAuthInterceptor implements HttpRequestInterceptor {

        /**
         * This method intercepts the connections to the server and provides the
         * authentication credentials if they have not been specified.
         *
         * @param request The intercepted request
         * @param context The context of the intercepted request
         * @throws HttpException, IOException
         */
        @Override
        public void process(final HttpRequest request, final HttpContext context) throws HttpException, IOException {
            AuthState authState = (AuthState) context.getAttribute(HttpClientContext.TARGET_AUTH_STATE);

            // If no auth scheme avaialble yet, try to initialize it
            // preemptively
            if (authState.getAuthScheme() == null) {
                AuthScheme authScheme = (AuthScheme) localContext.getAttribute("preemptive-auth");
                CredentialsProvider credsProvider = (CredentialsProvider) context.getAttribute(HttpClientContext.CREDS_PROVIDER);
                HttpHost targetHost = (HttpHost) context.getAttribute(HttpCoreContext.HTTP_TARGET_HOST);
                if (authScheme != null) {
                    AuthScope sc = new AuthScope(targetHost.getHostName(), targetHost.getPort());
                    Credentials creds = credsProvider.getCredentials(sc);
                    if (creds == null) {
                        throw new HttpException("No credentials for preemptive authentication");
                    }
                    authState.update(authScheme, creds);
                }
            }

        }
    }
}
