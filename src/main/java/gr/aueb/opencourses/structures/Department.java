/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

import java.util.List;

/**
 * This class describes a single department It contains the name of
 * the department, a list of courses in that department and the link where it can be found
 *
 * @author Midas
 */
public class Department {

    private List<Course> courses;
    private String name;
    private String link;

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}
