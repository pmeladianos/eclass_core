/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

/**
 *
 * @author pmeladianos
 */
public class Concept {
    
   String id;
   String keywords;

    public Concept(String id, String keywords) {
        this.id = id;
        this.keywords = keywords;
    }

   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
   
   
    
}