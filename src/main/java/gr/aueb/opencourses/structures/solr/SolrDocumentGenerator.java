/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures.solr;

import gr.aueb.opencourses.structures.Concept;
import gr.aueb.opencourses.structures.Course;
import gr.aueb.opencourses.structures.EclassDocument;
import gr.aueb.opencourses.structures.Journal;
import org.apache.solr.common.SolrInputDocument;

/**
 *
 * @author Midas
 */
public class SolrDocumentGenerator extends SolrInputDocument {

    public SolrInputDocument getSolrSchoolDocument(String eclass_school, String seed) {

        SolrInputDocument docsc = new SolrInputDocument();
        docsc.addField("id", eclass_school);
        docsc.addField("school_name_t", eclass_school);
        docsc.addField("faculty_t", "-");
        docsc.addField("course_name_t", "-");
        docsc.addField("teacher_name_t", "-");
        docsc.addField("link_s", seed);
        docsc.addField("document_name_t", "-");
        docsc.addField("type_t", "school");
        docsc.addField("content_t", "-");
        docsc.addField("fileserver_link_s", seed);
        Long unixTime = System.currentTimeMillis() / 1000;
        docsc.addField("timestamp_t", unixTime.toString());
        return docsc;

    }
     public SolrInputDocument getSolrConceptDocument(Concept c) {
        SolrInputDocument docc = new SolrInputDocument();
        docc.addField("id", c.getId());
        docc.addField("content_t", c.getKeywords());
        docc.addField("link_s", "https://en.wikipedia.org/wiki/Algorithm");
        docc.addField("fileserver_link_s", "https://en.wikipedia.org/wiki/Algorithm");
        docc.addField("type_t", "concept");
        return docc;
    }
        public SolrInputDocument getSolrJournal(Journal j) {

        SolrInputDocument docj = new SolrInputDocument();
        docj.addField("id", j.getId());
        docj.addField("school_name_t", j.getPublisher());
        docj.addField("faculty_t",  j.getIssn());
        docj.addField("course_name_t", j.getTitle());
        docj.addField("teacher_name_t", j.getClassification());
        docj.addField("document_name_t", j.getPid());
        docj.addField("content_t", j.getYears_available());
        docj.addField("link_s", j.getUrl());
        docj.addField("fileserver_link_s", j.getUrl());
        docj.addField("type_t", "journal");
        Long unixTime = System.currentTimeMillis() / 1000;
        docj.addField("timestamp_t", unixTime.toString());
        return docj;

    }


    public SolrInputDocument getSolrFacultyDocument(String eclass_school, String faculty, String faculty_link, String seed) {

        SolrInputDocument docf = new SolrInputDocument();
        docf.addField("id", faculty + "_" + eclass_school);
        docf.addField("school_name_t", eclass_school);
        docf.addField("faculty_t", faculty);
        docf.addField("course_name_t", "-");
        docf.addField("teacher_name_t", "-");
        docf.addField("link_s", faculty_link);
        docf.addField("document_name_t", "-");
        docf.addField("type_t", "faculty");
        docf.addField("content_t", "-");
        docf.addField("fileserver_link_s", faculty_link);
        Long unixTime = System.currentTimeMillis() / 1000;
        docf.addField("timestamp_t", unixTime.toString());
        return docf;

    }

    public SolrInputDocument getSolrCourseDocument(Course c) {

        SolrInputDocument docc = new SolrInputDocument();

        docc.addField("id", c.getName() + "_" + c.getFaculty() + "_" + c.getSchool());
        docc.addField("school_name_t", c.getSchool());
        docc.addField("faculty_t", c.getFaculty());
        docc.addField("course_name_t", c.getName());
        docc.addField("teacher_name_t", c.getTeacher());
        docc.addField("content_t", c.getCourseDescription());
        docc.addField("link_s", c.getLink());
        docc.addField("fileserver_link_s", c.getLink());
        docc.addField("type_t", "course");
        Long unixTime = System.currentTimeMillis() / 1000;
        docc.addField("timestamp_t", unixTime.toString());
        return docc;
    }

    public SolrInputDocument getSolrDocument(Course c,EclassDocument edoc) {
        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", edoc.getLink());
        doc.addField("school_name_t", c.getSchool());
        doc.addField("faculty_t", c.getFaculty());
        doc.addField("course_name_t", c.getName());
        doc.addField("teacher_name_t", c.getTeacher());
        doc.addField("link_s", edoc.getLink());
        doc.addField("document_name_t", edoc.getName());
        doc.addField("type_t", "document");
        doc.addField("content_t", edoc.getContent());
        doc.addField("keywords_t", edoc.getKeywords());
        Long unixTime = System.currentTimeMillis() / 1000;
        doc.addField("timestamp_t", unixTime.toString());
        doc.addField("fileserver_link_s", edoc.getFileServerlink());

        return doc;
    }

}
