/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

import java.util.List;

/**
 * This class describes a single school It contains the name of
 * the school, a list of departments in that school and the link where it can be found
 *
 * @author Midas
 */
public class School {
    private List<Department> departments;
     private String name;
     private String link;

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
     
     
}
