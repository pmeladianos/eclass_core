/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

import java.util.ArrayList;
import java.util.List;

/**
 *  This class describes a folder in the open Eclass. I contains the list of the ddocuments
 * in that folder and a list of sub-folders in that folder.
 * @author Midas
 */
public class EclassFolder {
    private List<EclassDocument> documents;
    private List<EclassFolder> folders;
    private String name;

    public EclassFolder() {
        documents=new ArrayList<EclassDocument>();
        folders=new ArrayList<EclassFolder>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public List<EclassFolder> getFolders() {
        return folders;
    }

    public void setFolders(List<EclassFolder> folders) {
        this.folders = folders;
    }
    
    public List<EclassDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<EclassDocument> documents) {
        this.documents = documents;
    }
    
    
}
