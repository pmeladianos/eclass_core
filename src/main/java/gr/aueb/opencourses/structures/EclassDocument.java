/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

import gr.aueb.kcore.KCore;
import gr.aueb.opencourses.eclass.crawler.Utils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.el.GreekAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.lang.Integer.min;

/**
 *This class describes a single document in open eClass. It includes the link in the original eClass
 * the link of the archived version in the fileServer and the name of the document.
 * @author Midas
 */
public class EclassDocument {
    private String type;
    private String link;
    private String FileServerlink;
    private String name;
    private String keywords;
    private String content;


    public String getFileServerlink() {
        return FileServerlink;
    }

    public void setFileServerlink(String FileServerlink) {
        this.FileServerlink = FileServerlink;
    }
    
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
        if (!content.isEmpty()){

            Analyzer an=new StandardAnalyzer();
            TokenStream enStream = an.tokenStream("content", content);
            CharTermAttribute enCattr = enStream.addAttribute(CharTermAttribute.class);
            String processedText = "";
            try {
                enStream.reset();
                while (enStream.incrementToken()) {
                    if(enCattr.toString().length()>3)
                        processedText+=enCattr.toString()+" ";
                }
                enStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String greekStopwords = Utils.greekStopwords.toLowerCase();
            List<String> stopwords = Arrays.asList(greekStopwords.split("\\n"));
            CharArraySet set=new CharArraySet(stopwords,true);
            GreekAnalyzer gr=new GreekAnalyzer(set);
            TokenStream grStream = gr.tokenStream("content", processedText);
            CharTermAttribute grCattr = grStream.addAttribute(CharTermAttribute.class);
            String processedTextGr = "";

            try {
                grStream.reset();
                while (grStream.incrementToken()) {
                    if(grCattr.toString().length()>3)
                        processedTextGr+=grCattr.toString()+" ";
                }
                grStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            List<String> result=new ArrayList<String>();

            KCore kcore=new KCore();
            Map<String, Double> keywords = kcore.kcoreDecomposition(processedTextGr,1);
            List<Object> topKeywords = (Arrays.asList(keywords.keySet().toArray()));
            topKeywords=topKeywords.subList(0,min(20,topKeywords.size()));
            String topKeys = topKeywords.stream()
                    .map(i -> i.toString())
                    .collect(Collectors.joining(" "));
            setKeywords(topKeys);
        }

    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    
}
