/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

/**
 *
 * @author pmeladianos
 */
public class Journal {

    private String id;
    private String issn;
    private String title;
    private String classification;
    private String url;
    private String pid;
    private String publisher;
    private String years_available;

    public Journal(String id, String issn, String title, String classification, String url, String pid, String publisher, String years_available) {
        this.id = id;
        this.issn = issn;
        this.title = title;
        this.classification = classification;
        this.url = url;
        this.pid = pid;
        this.publisher = publisher;
        this.years_available = years_available;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getYears_available() {
        return years_available;
    }

    public void setYears_available(String years_available) {
        this.years_available = years_available;
    }

   

}
