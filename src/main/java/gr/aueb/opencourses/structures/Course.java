/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.aueb.opencourses.structures;

import java.util.ArrayList;
import java.util.List;

/**
 * This class describes a single course It contains a description, the name of
 * the course a unique id, the teacher name and the link where it can be found
 *
 * @author Midas
 */
public class Course {
    private String courseDescription;
    private String name;
    private String id;
    private String teacher;
    private String link;
    private String faculty;
    private String school;

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
  
   // private List<String> announcements;
    private List<EclassDocument> documents;
    private List<EclassFolder> folders;
    
    
    public String getName() {
          return name;
      }

      public void setName(String name) {
          this.name = name;
      }
    public Course() {
        documents=new ArrayList<EclassDocument>();
        folders=new ArrayList<EclassFolder>();
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public List<EclassDocument> getDocuments() {
        return documents;
    }

    public void setDocuments(List<EclassDocument> documents) {
        this.documents = documents;
    }

    public List<EclassFolder> getFolders() {
        return folders;
    }

    public void setFolders(List<EclassFolder> folders) {
        this.folders = folders;
    }
    
}
